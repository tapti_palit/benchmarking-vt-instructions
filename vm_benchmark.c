#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/io.h>
#include "vm_benchmark.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Tapti Palit");
MODULE_DESCRIPTION("A module to benchmark vmlaunch instruction.");

char *vmxon_vir_page = NULL;
unsigned long int vmxon_phy_page = NULL;
char *vmcs_vir_page = NULL;
unsigned long int vmcs_phy_page = NULL;

char *msr_host_area_vir_page = NULL;
unsigned long int msr_host_area_phy_page = NULL;
char *msr_guest_area_vir_page = NULL;
unsigned long int msr_guest_area_phy_page = NULL;

int vmx_rev_id = 0; // To be read from the IA32_VMX_BASIC msr
int vmxon_success = 0;

int vmx_init_vmxon(void);
int vmcs_write(unsigned long, unsigned long);
void init_vmcs_guest(void);
void init_vmcs_host(void);
void init_vmcs_controls(void);
void vmx_read_capability_msr( unsigned long, unsigned int*, unsigned int*);

unsigned long do_vmread(unsigned long);

unsigned long do_vmread(unsigned long field) {
        unsigned long value;
	asm volatile ( "vmread %%rdx, %%rax;"
		      : "=a"(value) : "d"(field) : "cc");

	return value;
}

int vmcs_write(unsigned long field, unsigned long value) 
{
  unsigned char error = 0;
  asm volatile ("clc;"
		"vmwrite %1, %2;"
		"setna %0;"
		:"=q"(error) : "a"(value), "d"(field) : "cc");
  printk("Wrote to vmcs with error = %x\n", error);
  return error;

}

int vmx_init_vmxon(void) 
{
  unsigned long int cpuid_rcx = 0;
  unsigned long int msr_rax = 0;
  unsigned long int msr_rdx = 0;
  unsigned char vmxon_error = 0;
  unsigned char vmptrld_error = 0;
  unsigned char vmlaunch_error = 0;
  unsigned char vmclear_error = 0;
  unsigned char launch_error = 0;

  // Check if VMX is supported. 
  asm volatile(
	       "movq $1, %%rax;"
	       "cpuid;"
	       "movq %%rcx, %0;": "=r" (cpuid_rcx));
  printk("rcx = %ld\n", cpuid_rcx);
  if ((cpuid_rcx & 0x20) == 0x20) {
    printk("VMX is supported!\n");
  } else {
    printk("VMX is not supported!\n");
    return 1;
  }

  // Check bit 0 of the IA32_FEATURE_CONTROL MSR (MSR address 3AH).
  asm volatile(
	       "movq $0x3a, %%rcx;"
	       "rdmsr;"
	       "movq %%rax, %0;"
	       "movq %%rdx, %1;"
	       : "=r" (msr_rax), "=r" (msr_rdx));
  if (msr_rax & 1) {
    printk("Lock bit is clear!\n");
  } else {
    printk("Lock bit is set! VMX is not supported!\n");
    return 1;
  }
  if ((msr_rax >> 2) & 1) {
    printk("VMXON bit is set!\n");
  } else {
    printk("VMXON bit is clear! VMX is not supported!\n");
  }

  // Allocate a page as the VMXON region and write the vmcs revision id
  vmxon_vir_page = kmalloc(PAGE_SIZE, GFP_KERNEL);
  memset(vmxon_vir_page, 0, PAGE_SIZE);
  vmxon_phy_page = virt_to_phys(vmxon_vir_page);
	
  printk("The physical address of the vmxon region is = %x\n", vmxon_phy_page);
  // Setup the revision id.
  // Read the IA32_VMX_BASIC MSR (480h). The bits 30:0 contain the revision id
  // supported by the processor. 
	
  asm volatile(
	       "movq $0x480, %%rcx;"
	       "rdmsr;"
	       "movq %%rax, %0;"
	       "movq %%rdx, %1;"
	       : "=r" (msr_rax), "=r" (msr_rdx));
  vmx_rev_id = msr_rax;
  printk("The vmx revid supported by the processor is = %x\n", vmx_rev_id);
  // Write the revid to the vmxon region
  memcpy(vmxon_vir_page, &vmx_rev_id, 4);

  // Set the CR4.VMXE bit to 1.
  asm volatile("movq %cr4, %rax\n"
	       "bts $13, %rax\n"
	       "movq %rax, %cr4\n"
	       );
  printk("Successfully set CR4.VMXE bit!\n");

  // Do a VMXON.
  asm volatile("clc;"
	       "vmxon %1;"
	       "setna %0;"
	       :"=q"(vmxon_error):"m"(vmxon_phy_page):"cc");
  vmxon_success = !vmxon_error; 
  if (!vmxon_error) {
    printk("Successfully did VMXON!\n");
  } else {
    printk("Couldn't do VMXON! Error = %x\n", vmxon_error);
    return 1;
  }

  // Setup a vmcs region
  vmcs_vir_page = kmalloc(PAGE_SIZE, GFP_KERNEL);
  memset(vmcs_vir_page, 0, PAGE_SIZE);
  vmcs_phy_page = virt_to_phys(vmcs_vir_page);
  
  printk("Allocated vmcs region at physical address = %x\n", vmcs_phy_page);
  // Copy the revision id
  printk("Copying the vmx_rev_id = %x\n", vmx_rev_id);
  memcpy(vmcs_vir_page, &vmx_rev_id, 4);

  printk("Initialized the vmcs region with revision id!\n");

  // Allocate pages for the MSR regions in the VMCS
  msr_host_area_vir_page = kmalloc(PAGE_SIZE, GFP_KERNEL);
  msr_host_area_phy_page = virt_to_phys(msr_host_area_vir_page);
  memset(msr_host_area_vir_page, 0, PAGE_SIZE);
  msr_guest_area_vir_page = kmalloc(PAGE_SIZE, GFP_KERNEL);
  msr_guest_area_phy_page = virt_to_phys(msr_guest_area_vir_page);
  memset(msr_guest_area_vir_page, 0, PAGE_SIZE);

  // Do vmclear

  asm volatile("clc;"
	       "vmclear %1;"
	       "setna %0"
	       : "=q"(vmclear_error) : "m" ( vmcs_phy_page ) : "cc");

  if(!vmclear_error) {
    printk("vmclear succeeded!\n");
  } else {
    printk("vmclear failed!\n");
  }

  // Load the vmcs region
  asm volatile("clc;"
	       "vmptrld %1;"
	       "setna %0;"
	       :"=q"(vmptrld_error):"m"(vmcs_phy_page):"cc");
  
  if(!vmptrld_error) {
    printk("vmptrld succeeded!\n");
  } else {
    printk("vmptrld failed!\n");
  }

  // Now set up the VMCS region

  init_vmcs_guest();
  init_vmcs_host();
  // We don't need to setup IO or Exception bitmaps, because the guest will do only a vmcall.
  init_vmcs_controls();
  asm volatile("clc;"
	       "vmlaunch;"
	       "setna %0"
	       : "=q"( vmlaunch_error ) :: "cc");
  if (!vmlaunch_error) {
    printk("VMlaunch done successfully!\n");
  } else {
    launch_error = do_vmread(VMCS_32BIT_INSTRUCTION_ERROR);
    printk("VMlaunch failed with error code = %x\n", launch_error);
  }
  asm volatile(".Lguest_launch:");
  asm volatile(".Lvmx_return:");
  printk("This should never get printed!\n");
  asm volatile("host_continues:");
  printk("Returned to host!\n");
  return 0;
}

void init_vmcs_host(void)
{
  // Do things correctly here, else host'll stop working after a vmcall!

  unsigned long int cr0 = 0x0;
  unsigned long int cr3 = 0x0;
  unsigned long int cr4 = 0x0;

  unsigned short ds_reg = 0x0;
  unsigned short cs_reg = 0x0;

  unsigned long int tmpl;

  SegmentDescriptor interruptDescriptor;
  SegmentDescriptor globalDescriptor;
  unsigned short task_register;

  // The control registers
  asm volatile("movq %%cr0, %0;": "=r"(cr0));
  asm volatile("movq %%cr3, %0;": "=r"(cr3));
  asm volatile("movq %%cr4, %0;": "=r"(cr4));

  printk("Writing values of control registers %x, %x, %x to VMCS!\n", cr0, cr3, cr4);
  vmcs_write( VMCS_HOST_CR0, cr0); 
  vmcs_write( VMCS_HOST_CR3, cr3 ); 
  vmcs_write( VMCS_HOST_CR4, cr4 );

  // Store the DS and CS register values.

  asm volatile("mov %%cs, %0;": "=r"(cs_reg));
  asm volatile("mov %%ds, %0;": "=r"(ds_reg));

  printk("Writing values of the cs and ds as %x, %x\n", cs_reg, ds_reg);
  vmcs_write( VMCS_16BIT_HOST_ES_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_HOST_SS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_HOST_DS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_HOST_FS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_HOST_GS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_HOST_CS_SELECTOR, cs_reg );
    
  // Read the IDTR, GDTR and TR register
  asm volatile("sidt %0;" :: "m" (interruptDescriptor));
  vmcs_write( VMCS_HOST_IDTR_BASE, interruptDescriptor.base);

  printk("Read the IDT base as %x and limit as %x\n", interruptDescriptor.base, interruptDescriptor.limit);

  asm volatile("sgdt %0;" : : "m" (globalDescriptor));
  vmcs_write( VMCS_HOST_GDTR_BASE, globalDescriptor.base );
  printk("Read the GDT base as %x and limit as %x\n", globalDescriptor.base, globalDescriptor.limit);

  asm volatile("str %0;" : : "m" (task_register));
  printk("Writing value of TR selection as %x\n", task_register);
  vmcs_write( VMCS_16BIT_HOST_TR_SELECTOR, task_register);

  // FIXME! - Anything to be done here?
  vmcs_write( VMCS_HOST_FS_BASE, 0x0 );
  vmcs_write( VMCS_HOST_GS_BASE, 0x0 );
  // FIXME! - Is this correct? 
  // The haloed AMD manual says - 
  // Selector Index Field. Bits 15:3. The selector-index field specifies an entry in the descriptor table.
  // Descriptor-table entries are eight bytes long, so the selector index is scaled by 8 to form a byte offset
  // into the descriptor table. The offset is then added to either the global or local descriptor-table base
  // address (as indicated by the table-index bit) to form the descriptor-entry address in virtual-address
  // space.
  vmcs_write( VMCS_HOST_TR_BASE, *((unsigned long*)phys_to_virt(globalDescriptor.base + 8*(task_register>>3))));

  asm("movabs $.Lvmx_return, %0" : "=r"(tmpl));
  vmcs_write(VMCS_HOST_RIP, tmpl);

}

void init_vmcs_guest(void) 
{
  // We don't want an unrestricted guest. So we'll copy the host's values in this and cross our fingers!
  unsigned long int cr0 = 0x0;
  unsigned long int cr3 = 0x0;
  unsigned long int cr4 = 0x0;

  unsigned short ds_reg = 0x0;
  unsigned short cs_reg = 0x0;

  unsigned short ts_reg = 0x0;

  unsigned long int tmpl;

  SegmentDescriptor interruptDescriptor;
  SegmentDescriptor globalDescriptor;
  unsigned short task_register;

  // The control registers
  asm volatile("movq %%cr0, %0;": "=r"(cr0));
  asm volatile("movq %%cr3, %0;": "=r"(cr3));
  asm volatile("movq %%cr4, %0;": "=r"(cr4));

  vmcs_write( VMCS_HOST_CR0, cr0); 
  vmcs_write( VMCS_HOST_CR3, cr3 ); 
  vmcs_write( VMCS_HOST_CR4, cr4 );

  // Store the DS and CS register values.

  asm volatile("mov %%cs, %0;": "=r"(cs_reg));
  asm volatile("mov %%ds, %0;": "=r"(ds_reg));

  vmcs_write( VMCS_16BIT_GUEST_CS_SELECTOR, cs_reg );
  vmcs_write( VMCS_16BIT_GUEST_ES_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_GUEST_SS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_GUEST_DS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_GUEST_FS_SELECTOR, ds_reg );
  vmcs_write( VMCS_16BIT_GUEST_GS_SELECTOR, ds_reg );
  
  // FIXME - Do we need this too? 
  vmcs_write( VMCS_16BIT_GUEST_TR_SELECTOR, 0x0 );
  vmcs_write( VMCS_16BIT_GUEST_LDTR_SELECTOR, 0x0 );

  vmcs_write( VMCS_GUEST_CS_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_ES_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_SS_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_DS_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_FS_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_GS_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_LDTR_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_GDTR_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_IDTR_BASE, 0x0 );
  vmcs_write( VMCS_GUEST_TR_BASE, 0x0 );

  vmcs_write( VMCS_32BIT_GUEST_CS_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_ES_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_SS_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_DS_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_FS_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_GS_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_LDTR_LIMIT, 0x0000FFFF );
  vmcs_write( VMCS_32BIT_GUEST_TR_LIMIT, 0xFFFFF );
  vmcs_write( VMCS_32BIT_GUEST_GDTR_LIMIT, 0x30 );
  vmcs_write( VMCS_32BIT_GUEST_IDTR_LIMIT, 0x3FF );
  // FIXME: Fix access rights.
  vmcs_write( VMCS_32BIT_GUEST_CS_ACCESS_RIGHTS, 0x93 );
  vmcs_write( VMCS_32BIT_GUEST_ES_ACCESS_RIGHTS, 0x93 );
  vmcs_write( VMCS_32BIT_GUEST_SS_ACCESS_RIGHTS, 0x93 );
  vmcs_write( VMCS_32BIT_GUEST_DS_ACCESS_RIGHTS, 0x93 );
  vmcs_write( VMCS_32BIT_GUEST_FS_ACCESS_RIGHTS, 0x93 );
  vmcs_write( VMCS_32BIT_GUEST_GS_ACCESS_RIGHTS, 0x93 );
  vmcs_write( VMCS_32BIT_GUEST_LDTR_ACCESS_RIGHTS, 0x82 );
  vmcs_write( VMCS_32BIT_GUEST_TR_ACCESS_RIGHTS, 0x8b );

  vmcs_write( VMCS_32BIT_GUEST_ACTIVITY_STATE, 0 );
  vmcs_write( VMCS_32BIT_GUEST_INTERRUPTIBILITY_STATE, 0 );

  vmcs_write( VMCS_GUEST_CR3, cr3 );
  vmcs_write( VMCS_GUEST_CR0, cr0);
  vmcs_write( VMCS_GUEST_CR4, cr4);

  vmcs_write( VMCS_64BIT_GUEST_LINK_POINTER, 0xffffffff );
  vmcs_write( VMCS_64BIT_GUEST_LINK_POINTER_HI, 0xffffffff ); 
  vmcs_write( VMCS_GUEST_DR7, 0x0 );
  vmcs_write( VMCS_GUEST_RFLAGS, 0x2 );

  asm("movabs $.Lguest_launch, %0" : "=r"(tmpl));
  vmcs_write( VMCS_GUEST_RIP, tmpl); 
}

void vmx_read_capability_msr( unsigned long msr, unsigned int* hi, unsigned int* lo ) {
  unsigned long int msr_rax = 0x0;
  unsigned long int msr_rdx = 0x0;
  asm volatile(
	       "movq %2, %%rcx;"
	       "rdmsr;"
	       "movq %%rax, %0;"
	       "movq %%rdx, %1;"
	       : "=r" (msr_rax), "=r" (msr_rdx): "r" (msr));
    *hi = msr_rdx;
    *lo = msr_rax;
    printk("The hi value is = %x\n", *hi);
    printk("The lo value is = %x\n", *lo);
}

void init_vmcs_controls()
{
  unsigned int pinbased_ctls_zero, pinbased_ctls_one;
  unsigned int procbased_ctls_zero, procbased_ctls_one;
  unsigned int procbased_ctls2_zero, procbased_ctls2_one;
  unsigned int exit_ctls_zero, exit_ctls_one;
  unsigned int entry_ctls_zero, entry_ctls_one;

  printk("Initializing vmcs control bits!\n");

  // Set pin based vm exec controls.
  vmx_read_capability_msr( IA32_VMX_PINBASED_CTLS, 
			   &pinbased_ctls_one, &pinbased_ctls_zero );

  vmcs_write( VMCS_32BIT_CONTROL_PIN_BASED_EXEC_CONTROLS, 
	      (~pinbased_ctls_zero) & pinbased_ctls_one );

  // Set proc-based controls.
  vmx_read_capability_msr( IA32_VMX_PROCBASED_CTLS, 
			   &procbased_ctls_one, &procbased_ctls_zero );
   
  vmcs_write( VMCS_32BIT_CONTROL_PROCESSOR_BASED_VMEXEC_CONTROLS, 
	      (~procbased_ctls_zero) & procbased_ctls_one );

  // Set Proc based secondary controls.
  vmx_read_capability_msr( IA32_VMX_PROCBASED_CTLS2, 
			   &procbased_ctls2_one, &procbased_ctls2_zero );
    
  // Set VM exit controls.
  vmx_read_capability_msr( IA32_VMX_EXIT_CTLS, 
			   &exit_ctls_one, &exit_ctls_zero );

  vmcs_write( VMCS_32BIT_CONTROL_VMEXIT_CONTROLS, 
	      (~exit_ctls_zero) & exit_ctls_one );
  
  // Set VM entry controls.
  vmx_read_capability_msr( IA32_VMX_ENTRY_CTLS, 
			   &entry_ctls_one, &entry_ctls_zero );
  vmcs_write( VMCS_32BIT_CONTROL_VMENTRY_CONTROLS, 
	      (~entry_ctls_zero) & entry_ctls_one );

  /* unsigned long ept_ptr = e->env_cr3 | ( ( EPT_LEVELS - 1 ) << 3 ); */
  /* vmcs_write( VMCS_64BIT_CONTROL_EPTPTR, ept_ptr ); */

}

static int __init vm_benchmark_init(void)
{
  printk(KERN_INFO "Initing module!\n");
  vmx_init_vmxon();
  return 0;
}

static void __exit vm_benchmark_exit(void)
{
  if (vmxon_success) {
    printk("Going to do vmxoff!\n");
    //    asm volatile("vmxoff;");
    printk("Did vmxoff!\n");
  }
  if (!vmxon_vir_page) {
    kfree(vmxon_vir_page);
    }
  printk(KERN_INFO "Exiting module.\n");
}

module_init(vm_benchmark_init);
module_exit(vm_benchmark_exit);
